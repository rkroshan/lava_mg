#!/bin/sh

init_services() {
    echo "* Starting ser2net"
    ser2net -v
    echo  "ser2net restarting" 
    /etc/init.d/ser2net restart
    ser2net -d -c /etc/ser2net.conf
    sleep 1
}

init_services
