#!/bin/sh

set -ex

lava-server manage users list | grep -q admin || lava-server manage users add --staff --superuser --email admin@example.com --passwd admin admin

#example
#lava-server manage users list | grep -q <user-name> || lava-server manage users add --staff --superuser --email <email-address> --passwd <password> <user-name>
